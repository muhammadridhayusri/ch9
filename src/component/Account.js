import React, { useEffect, useState } from 'react'
import { UserAuth } from '../context/AuthContext'
import { Button, Card, Form } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'
import { Link } from 'react-router-dom'


export default function Account() {
  const {user, logout} = UserAuth()
  const navigate = useNavigate()

  async function handleSignOut(e) {
    e.preventDefault()
    try {
      await logout()
      navigate('/')
      console.log("you have log out")
    } catch (error) {
      console.log(error)
    }
  }

  useEffect((e) => {
    try {
      if(!user) {
        console.log('Log in First or Sign Up')
        navigate('/')
      }
  
    } catch (error) {
      console.log(error)
    }
  })

  return (
    <div>
    <div className='d-flex' >
    <Card>
      <Card.Body >
        <Form onSubmit={handleSignOut}>
        <Form.Group>
          <h1 className='text-center mb-5 '> Welcome Users </h1>
          <p className='w-500 text-center'> user Email : {user && user.email} </p>
          <Button type='submit' className='w-200'>Logout</Button>
        </Form.Group>
        </Form>
        
      </Card.Body>
    </Card>
    </div>
    <div className='d-flex' style={{marginTop:"40px"}}>
    <Card>
      <Card.Body>
      <Form>
          <Form.Group>
          <Link to={"/playgame"}><Button type="button" className='w-200'>Play Game</Button></Link>
          </Form.Group>
        </Form>
      </Card.Body>
      <Card.Body>
        <h1> RPS</h1>
      </Card.Body>
    </Card>
    </div>
    </div>
  )
}
