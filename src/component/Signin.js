import React, {useRef, useState} from 'react'
import { Card, Form, Button, Container, Alert } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { UserAuth } from '../context/AuthContext'
import { useNavigate } from 'react-router-dom'

export default function Signin() {

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const {signIn} = UserAuth()
  const navigate = useNavigate()

  async function handleSignin (e) {
   e.preventDefault()
   try {
      console.log(email,password)
      await signIn(email, password)
      console.log("You have Sign In")
      navigate('/account')
   } catch (error) {
     console.log("wrong email or password")
   }
  }

  return (
    <Container style={{maxWidth: "600px"}}>
    <Card>
         <Card.Body>
            <h2 className='text-center mb-4'>
               Sign In
            </h2>
            <Form onSubmit={handleSignin}>
               <Form.Group id='email'>
                  <Form.Label>Email</Form.Label>
                  <Form.Control type="email" onChange={(e) => setEmail(e.target.value)} required></Form.Control>
               </Form.Group>
               <Form.Group id='password'>
                  <Form.Label>Password</Form.Label>
                  <Form.Control type="password" onChange={(e) => setPassword(e.target.value)} required></Form.Control>
               </Form.Group>
               <Button type='submit' className='w-100'> Sign In</Button>
            </Form>
         </Card.Body>
      </Card>
      <p className='text-center'> Dont have account ? <Link to="/signup" className='underline'> Sign Up</Link> </p>
      </Container>
  )
}
