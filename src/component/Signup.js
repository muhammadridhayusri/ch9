import React from 'react'
import {Link} from 'react-router-dom'
import Register from '../pages/register'
import { Container } from 'react-bootstrap'

export default function Signup() {
  return (
    <>
      
      <Container className="mt-10 d-flex align-item-center justify-content-center" style={{minHeight:"100vh"}}>
         <div className="w-100" style={{ maxWidth: "600px"}}>
            <Register/>
            <p className='text-center'>
            Already have an account yet ? <Link to='/' className='bold'> Sign in.</Link>
            </p>
         </div>
      </Container>
      
    </>
  )
}
