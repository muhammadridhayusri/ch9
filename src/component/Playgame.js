import React, { useEffect, useState } from 'react'
import {db} from '../service/firebase'
import {collection, getDocs} from "firebase/firestore"

const Playgame = () => {
   const [user, setUser] = useState()
   const usersCollectionRef = collection(db, "users")

   useEffect(() => {
      
      const getUser = async() => {
         const data = await getDocs(usersCollectionRef)
         setUser(data.docs.map((doc) => ({...doc.data(), id: doc.id})))
         
      }
      getUser()
      console.log(user)
   },[])

   return (
    <div>Playgame</div>
  )
}

export default Playgame