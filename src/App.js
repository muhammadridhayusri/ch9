import React from "react";
import "./pages/register"
import Register from "./pages/register";
import {Container, Nav, Navbar} from 'react-bootstrap'
import Account from './component/Account'
import Signup from "./component/Signup";
import Signin from './component/Signin'
import { Routes, Route } from  'react-router-dom'
import { AuthContextProvider } from "./context/AuthContext";
import Playgame from "./component/Playgame";

function App() {
  return (
    <>
    <Navbar style={{marginBottom:"20px"}} className="me-auto" bg="primary" variant="dark">
    <Container className="mb-3">
      <Navbar.Brand href="/">Group II</Navbar.Brand>
      <Nav className="me-auto">
        <Nav.Link href="/">Sign In</Nav.Link>
        <Nav.Link href="/signup">Sign Up</Nav.Link>
      </Nav>
    </Container>
     </Navbar>
    <Container className="d-flex align-item-center justify-content-center"> 
    <AuthContextProvider>
      <Routes>
        <Route path ='/' element={<Signin/>} />
        <Route path ='/signup' element={<Signup/>} />
        <Route path ='/account' element={<Account/>} />
        <Route path ='/playgame' element={<Playgame/>} />
      </Routes>
    </AuthContextProvider>
    </Container>
    
    </>
  );
}

export default App;
