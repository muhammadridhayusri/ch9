import * as firebase from "firebase/app"
import {getAuth} from 'firebase/auth'
import {getFirestore} from '@firebase/firestore'

const app = firebase.initializeApp({
  apiKey: "AIzaSyAqpH0PzpLOpq8_fwILr_fDxjKb4WYnHCs",
  authDomain: "fsw22binarkelompok2.firebaseapp.com",
  projectId: "fsw22binarkelompok2",
  storageBucket: "fsw22binarkelompok2.appspot.com",
  messagingSenderId: "1005634064219",
  appId: "1:1005634064219:web:ce0d837321f901a9cf547e"
})

export const auth = getAuth(app)
export const db = getFirestore(app)
export default app