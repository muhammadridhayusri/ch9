import React, {useState } from 'react'
import { Form, Button, Card } from 'react-bootstrap'
import {UserAuth} from '../context/AuthContext'
import { useNavigate } from 'react-router-dom'

export default function Register() {

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState('')
   const {createUser} = UserAuth()
   const navigate = useNavigate()
   
  async function handleSubmit(e){
   e.preventDefault()
   
   try {
      await createUser(email,password)
      console.log("Your Account Has Been Made")
      navigate('/account')
   } catch (e) {
      setError(e.message)
      console.log(e.message)
   }
  }

  return (
    <>
      <Card>
         <Card.Body>
            <h2 className='text-center mb-4'>
               SignUp
            </h2>
            <Form onSubmit={handleSubmit}>
               <Form.Group id='email'>
                  <Form.Label>Email</Form.Label>
                  <Form.Control type="email" onChange={(e) => setEmail(e.target.value)} required></Form.Control>
               </Form.Group>
               <Form.Group id='password'>
                  <Form.Label>Password</Form.Label>
                  <Form.Control type="password" onChange={(e) => setPassword(e.target.value)} required></Form.Control>
               </Form.Group>
               <Button type='submit' className='w-100'> Sign Up</Button>
            </Form>
         </Card.Body>
      </Card>
      
    </>
  )
}
